import { api as Transforms } from '../transformers/to-celsius.js'

const dt = ts => ts * 1000

const tempToAxis = r =>
	r.data.weatherByCity.Forecast.Weather.map(v => ({
		x: dt(v.dt),
		y: Transforms.transform(v.temperature),
		marker: {
			symbol: `url(http://openweathermap.org/img/w/${v.icon}.png)`
		}
	}))

const rainToAxis = r => r.data.weatherByCity.Forecast.Weather.map(v => [dt(v.dt), v.rain_volume])
const snowToxAxis = r => r.data.weatherByCity.Forecast.Weather.map(v => [dt(v.dt), v.snow_volume])
const humidityToAxis = r => r.data.weatherByCity.Forecast.Weather.map(v => [dt(v.dt), v.humidity / 100])

const api = {
	temp: tempToAxis,
	rain: rainToAxis,
	snow: snowToxAxis,
	humidity: humidityToAxis
}

export { api }
