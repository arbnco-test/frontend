import gql from 'graphql-tag'

const Query = gql`
	query GetWeather($city: String!, $unique: [String!]) {
		weatherByCity(city: $city, unique: $unique) {
			uuid
			timestamp
			Forecast {
				City
				Weather {
					day
					temperature
					dt
					icon
					description
					pressure
					humidity
					rain_volume
					snow_volume
				}
			}
		}
	}
`

export { Query }
