import { api as ToAxis } from './mappers.js'
import { Query } from './query.js'

const queryAndDisplayChart = function(city, apollo, $scope) {
	apollo
		.query({
			variables: { city: city },
			query: Query
		})
		.then(result => {
			console.log(result.data.weatherByCity)
			$scope.query.url = result.data.weatherByCity.uuid
			window.Highcharts.chart('container', {
				chart: {
					scrollablePlotArea: {
						minWidth: 700
					},
					zoomType: 'x' //Making x-axis zoomable/scrollable
				},

				title: {
					text: ''
				},

				subtitle: {
					text: ''
				},

				xAxis: {
					type: 'datetime'
				},

				yAxis: [
					{
						// left y axis
						title: {
							text: null
						},
						labels: {
							align: 'left',
							x: 3,
							y: 16,
							format: '{value:.,0f}'
						},
						showFirstLabel: false
					},
					{
						// right y axis
						linkedTo: 0,
						gridLineWidth: 0,
						opposite: true,
						title: {
							text: null
						},
						labels: {
							align: 'right',
							x: -3,
							y: 16,
							format: '{value:.,0f}'
						},
						showFirstLabel: false
					}
				],

				legend: {
					align: 'left',
					verticalAlign: 'top',
					borderWidth: 0
				},

				tooltip: {
					shared: true,
					crosshairs: true
				},

				plotOptions: {
					plotOptions: {
						spline: {
							marker: {
								radius: 4,
								lineColor: '#666666',
								lineWidth: 1
							}
						}
					}
				},

				series: [
					{
						name: 'Temp',
						lineWidth: 4,
						marker: {
							radius: 4
						},
						data: ToAxis.temp(result)
					},
					{
						name: 'Rain',
						data: ToAxis.rain(result)
					},
					{
						name: 'Humidity',
						data: ToAxis.humidity(result)
					}
				]
			})

			// Highcharts
		})
		.catch(err => console.log(err))
}

const api = {
	chart: queryAndDisplayChart
}

export { api }
