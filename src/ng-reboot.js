export function reboot(doc, getRootNode) {
  var lastRoot = getRootNode();
  var nextRoot = lastRoot.cloneNode(true);
  var insertionPoint = lastRoot.parentNode;
  if (lastRoot) {
    lastRoot.parentNode.removeChild(lastRoot);
  }
  insertionPoint.appendChild(nextRoot);
  return getRootNode();
}