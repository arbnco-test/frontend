import React from 'react'
// import { shallow } from 'enzyme'
import { api as Transform } from './to-12hour.js'

import { Assert, CatchAsFalse } from '../utils/assert.js'

describe('Transform - To12Hour:Pre()', () => {
	it('precondition fails with number: 9.99', () => {
		expect(() => Transform.pre(9.99)).toThrowError()
	})
	it("precondition fails without a colon: '1200'", () => {
		expect(() => Transform.pre('1200')).toThrowError()
	})
	it("precondition fails with wrong time format: '12:00:00'", () => {
		expect(() => Transform.pre('12:00:00')).toThrowError()
	})
	it("precondition fails with time:decimal format: '12:99'", () => {
		expect(() => Transform.pre('12:99')).toThrowError()
	})
	it("precondition passes with correct time format: '12:00'", () => {
		expect(Transform.pre('12:00')).toBeUndefined()
	})
})

describe('Transform - To12Hour:Fn()', () => {
	it('precondition passes with correct time format: 12', () => {
		expect(Transform.fn('12:00')).toEqual(12)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.fn('00:00')).toEqual(0)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.fn('0:00')).toEqual(0)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.fn('0:0')).toEqual(0)
	})
	it('precondition passes with correct time format: 3', () => {
		expect(Transform.fn('03:00')).toEqual(3)
	})
	it('precondition passes with correct time format: 3', () => {
		expect(Transform.fn('3:00')).toEqual(3)
	})
	it('precondition passes with correct time format: 9', () => {
		expect(Transform.fn('21:00')).toEqual(9)
	})
	it('precondition passes with correct time format: 9', () => {
		expect(Transform.fn('021:00')).toEqual(9)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.fn('17:30')).toEqual(5)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.fn('017:30')).toEqual(5)
	})
	//TODO: Openweather forecasts are always on the hour
	// if this changes you might want to inspect the
	// minutes to do rounding
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.fn('17:35')).toEqual(5)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.fn('017:35')).toEqual(5)
	})
})

describe('Transform - To12Hour:Post() with fallback', () => {
	it('precondition fails with number: 9.99 => fallback 0', () => {
		expect(Transform.transform(9.99)).toEqual(0)
	})
	it("precondition fails without a colon: '1200' => fallback 0", () => {
		expect(Transform.transform('1200')).toEqual(0)
	})
	it("precondition fails with wrong time format: '12:00:00' => fallback 0", () => {
		expect(Transform.transform('12:00:00')).toEqual(0)
	})
	it("precondition fails with time:decimal format: '12:99' => fallback 0", () => {
		expect(Transform.transform('12:99')).toEqual(0)
	})
})

describe('Transform - To12Hour:transform()', () => {
	it('precondition passes with correct time format: 12', () => {
		expect(Transform.transform('12:00')).toEqual(12)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.transform('00:00')).toEqual(0)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.transform('0:00')).toEqual(0)
	})
	it('precondition passes with correct time format: 0', () => {
		expect(Transform.transform('0:0')).toEqual(0)
	})
	it('precondition passes with correct time format: 3', () => {
		expect(Transform.transform('03:00')).toEqual(3)
	})
	it('precondition passes with correct time format: 3', () => {
		expect(Transform.transform('3:00')).toEqual(3)
	})
	it('precondition passes with correct time format: 9', () => {
		expect(Transform.transform('21:00')).toEqual(9)
	})
	it('precondition passes with correct time format: 9', () => {
		expect(Transform.transform('021:00')).toEqual(9)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.transform('17:30')).toEqual(5)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.transform('017:30')).toEqual(5)
	})
	//TODO: Openweather forecasts are always on the hour
	// if this changes you might want to inspect the
	// minutes to do rounding
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.transform('17:35')).toEqual(5)
	})
	it('precondition passes with correct time format: 5', () => {
		expect(Transform.transform('017:35')).toEqual(5)
	})
})
