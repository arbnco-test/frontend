import { Assert } from '../utils/assert.js'

// TimeToTwelveHour
// input '12:00'
// splits a string and returns a twelvehour formated integer

const fallback = 0

const errShouldBeString = 'we expected a string but got a different type'
const shouldBeString = s => typeof s === 'string'

const errShouldContainColon = 'expected time format to be in the format 12:00'
const shouldContainColon = s => s.indexOf(':') !== -1

const errShouldBeInMinutes = 'expected time format to be in the format 12:00 lookes like a time:dec format'
const shouldBeInMinutes = s => parseInt(s.split(':')[1], 10) < 61

const errShouldBeCorrectLength = 'expected time to be in the format have you used two colons instead of one 12:00:00'
const shouldBeCorrectLength = s => s.split(':').length > 1 && s.split(':').length < 3

const api = {
	pre: s => {
		Assert(shouldBeString)(s, errShouldBeString)
		Assert(shouldContainColon)(s, errShouldContainColon)
		Assert(shouldBeCorrectLength)(s, errShouldBeCorrectLength)
		Assert(shouldBeInMinutes)(s, errShouldBeInMinutes)
	},
	fn: s => {
		var i = parseInt(s.split(':')[0], 10)
		return i > 12 ? i - 12 : i
	},
	transform: s => {
		try {
			api.pre(s)
			return api.fn(s)
		} catch (err) {
			return fallback
		}
	}
}

export { api }
