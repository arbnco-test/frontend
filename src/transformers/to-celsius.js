import { Assert, Throw } from '../utils/assert'

// KelvinToCelcius
// standard formula for temperature conversion
// We round the result to a whole number

const fallback = 0

const errShouldNotBeZero = 'below absolute zero (0 K)'
const shouldNotBeZero = num => !(num <= 273.15)

const shouldNotBeNegZero = n => !Object.is(-0, n)
const errshouldNotBeNegZero = 'rounded result is negative zero -0'

const api = {
	pre: num => Assert(shouldNotBeZero)(num, errShouldNotBeZero),
	fn: num => Math.round(num - 273.15),
	post: num => (shouldNotBeNegZero(num) ? num : Throw(errshouldNotBeNegZero)),
	transform: num => {
		try {
			api.pre(num)
			return api.post(api.fn(num))
		} catch (err) {
			return fallback
		}
	}
}

export { api }
