import { Assert } from '../utils/assert.js'
import { api as ToCelcius } from './to-celsius.js'
import { api as To12Hour } from './to-12hour.js'

const fallback = {
	cloud_cover: 100,
	condition: 100,
	day: 'Error',
	description: 'Something Wrong',
	temperature: 274.15,
	time_friendly: '3:00',
	wind_direction: 180
}

const errShouldHaveProperties = `should have the following properties 'day', 'temperature', 'condition', 'description', 'cloud_cover', 'wind_direction' check all data was returned from the api`
const shouldHaveProperties = obj => {
	let keys = ['cloud_cover', 'condition', 'day', 'description', 'temperature', 'time_friendly', 'wind_direction']
	return keys.every(key => Object.keys(obj).indexOf(key) > -1)
}

const api = {
	pre: obj => Assert(shouldHaveProperties)(obj, errShouldHaveProperties),
	fn: obj => {
		let props = Object.assign({}, obj)
		props.temperature = ToCelcius.transform(obj.temperature)
		props.time_friendly = To12Hour.transform(obj.time_friendly)
		return props
	},
	transform: obj => {
		try {
			api.pre(obj)
			return api.fn(obj)
		} catch (err) {
			return fallback
		}
	}
}

export { api }
