import React from 'react'
// import { shallow } from 'enzyme'
import { api as Transform } from './to-celsius.js'

import { Assert } from '../utils/assert.js'

describe('Transform - Celsius:Pre()', () => {
	it('precondition fails with absolute zero', () => {
		expect(() => Transform.pre(273.15)).toThrowError()
	})
	it('precondition fails with just shy or absolute zero', () => {
		expect(() => Transform.pre(273)).toThrowError()
	})
	it('precondition fails with negative number', () => {
		expect(() => Transform.pre(-10)).toThrowError()
	})
	it('precondition passes with a number in range', () => {
		expect(Transform.pre('280')).toBeUndefined()
	})
})

describe('Transform - Celsius:Transform() => fallback 0', () => {
	it('precondition fails with absolute zero', () => {
		expect(Transform.transform(273.15)).toEqual(0)
	})
	it('precondition fails with just shy or absolute zero', () => {
		expect(Transform.transform(273)).toEqual(0)
	})
	it('precondition fails with negative number', () => {
		expect(Transform.transform(-10)).toEqual(0)
	})
	it('precondition fails with negative number', () => {
		expect(Transform.transform(280)).toEqual(7)
	})
})

describe('Transform - Celsius:Fn() actual values', () => {
	it('fn should equal 0', () => {
		expect(Transform.fn(273.15)).toEqual(0)
	})
	it('fn should equal -0', () => {
		expect(Transform.fn(273)).toEqual(-0)
	})
	it('should equal -283', () => {
		expect(Transform.fn(-10)).toEqual(-283)
	})
	it('should equal 7', () => {
		expect(Transform.fn(280)).toEqual(7)
	})
})

describe('Transform - Celsius:Post() actual values', () => {
	it('if input is -0 it will throw an error', () => {
		expect(() => Transform.post(-0)).toThrowError()
	})
	it('if input is 0 output is 0', () => {
		expect(Transform.post(0)).toEqual(0)
	})
	it('if input is 10 output is 10', () => {
		expect(Transform.post(10)).toEqual(10)
	})
})
