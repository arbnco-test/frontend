import React from 'react'
import { api as Transform } from './to-props.js'

describe('Transform - ToProps:Pre()', () => {
	it('precondition fails with object with not all keys', () => {
		expect(() =>
			Transform.pre({
				cloud_cover: 100,
				useless: 'the rest of the keys are missing'
			})
		).toThrowError()
	})
	it('precondition succeeds with object with all keys', () => {
		expect(() =>
			Transform.pre({
				cloud_cover: 100,
				condition: 100,
				day: 'Error',
				description: 'Something Wrong',
				temperature: 274.15,
				time_friendly: '3:00',
				wind_direction: 180
			})
		).not.toThrowError()
	})
})

describe('Transform - ToProps:Transform()', () => {
	it('transform returns props into another variable', () => {
		const obj = {
			cloud_cover: 100,
			condition: 100,
			day: 'Error',
			description: 'Something Wrong',
			temperature: 274.15,
			time_friendly: '15:00',
			wind_direction: 180
		}

		const transform = Transform.transform(obj)
		expect(transform.time_friendly).toEqual(3)
	})

	it('transform returns props with correct time', () => {
		expect(
			Transform.transform({
				cloud_cover: 100,
				condition: 100,
				day: 'Error',
				description: 'Something Wrong',
				temperature: 274.15,
				time_friendly: '3:00',
				wind_direction: 180
			}).time_friendly
		).toEqual(3)
	})

	it('transform returns props with correct temperature', () => {
		expect(
			Transform.transform({
				cloud_cover: 100,
				condition: 100,
				day: 'Error',
				description: 'Something Wrong',
				temperature: 280,
				time_friendly: '15:00',
				wind_direction: 180
			}).temperature
		).toEqual(7)
	})
	it('precondition fails transform returns fallback object', () => {
		expect(
			Transform.transform({
				cloud_cover: 100,
				useless: 'the rest of the keys are missing'
			}).day
		).toEqual('Error')
	})
})
