import './css/style.scss'
import AngularApollo from 'angular1-apollo'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import angular from 'angular'
import { api as Chart } from './chart/index.js'

angular
	.module('app', [AngularApollo])
	.config(apolloProvider => {
		apolloProvider.defaultClient(
			new ApolloClient({
				link: new HttpLink({ uri: 'https://srv-graphql-frontend-dot-arbn-232813.appspot.com/query' }),
				cache: new InMemoryCache()
			})
		)
	})
	.controller('MyController', [
		'$scope',
		'apollo',

		function($scope, apollo) {
			// Set Defaults
			$scope.query = { city: 'Glasgow' }

			// Search Function
			$scope.search = function(e) {
				console.log(e)
				if (e.keyCode === 13) {
					Chart.chart($scope.query.city, apollo, $scope)
				}
			}
		}
	])

// Manually bootstrap your angular application
angular.bootstrap(document.getElementById('root'), ['app'])
